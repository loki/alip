# Copyright 2011 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user='keenerd' ]

export_exlib_phases src_prepare src_install

SUMMARY="JSON parser designed for maximum convenience within the shell"
DESCRIPTION="
Jshon parses, reads and creates JSON. It is designed to be as usable as possible
from within the shell and replaces fragile adhoc parsers made from grep/sed/awk.

Jshon loads json text from stdin, performs actions, then dumps to stdout. Some
of the options return json, others output meta information. Because Bash has
very poor nested datastructures, Jshon does not try to return a native bash
datastructure containing the json. Instead, Jshon provides a history stack
containing all the manipulations.
"
HOMEPAGE="http://kmkeen.com/${PN}/"

LICENCES="MIT"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-libs/jansson
"

BUGS_TO="alip@exherbo.org"

jshon_src_prepare() {
    default

    # -Werror is bad mmkay?
    edo sed -i \
            -e '/^CFLAGS/s/-Werror//' \
            Makefile
}

jshon_src_install() {
    dobin ${PN}
    doman ${PN}.1
}

